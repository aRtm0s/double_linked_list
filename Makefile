all: main

main:
	gcc main.c double_linked_list.c double2bin_manip.c -Wall -o main.out -lpthread

clean:
	rm *.out

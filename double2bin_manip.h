#ifndef DOUBLE2BIN_MANIP_H
#define DOUBLE2BIN_MANIP_H

#include "stdlib.h"
#include <stdio.h>

typedef union _udouble {
    double v;        // double representation of value
    __uint64_t bin;   // binary representation of value
} udouble;

int binDigitCounter (double* value, int flag);

#endif // DOUBLE2BIN_MANIP_H
#include <stdio.h>
#include "double_linked_list.h"
#include "double2bin_manip.h"

DoubleLinkedList* createDoubleLinkedList(){
    DoubleLinkedList* temp_list = (DoubleLinkedList*) malloc(sizeof(DoubleLinkedList));

    // Memory allocation check
    if (temp_list == NULL) {
        fprintf(stderr, "Error in %s: Memory allocation failed\n", __func__);
        exit(1);
    }

    // Setting initial values for pointers and size of the list
    temp_list->size =  0;
    temp_list->head = NULL;
    temp_list->tail = NULL;

    return temp_list;
}

void deleteDoubleLinkedList(DoubleLinkedList** list) {
    ListNode* temp_node = (*list)->head;
    
    if (temp_node == NULL) {
        free(*list);
        (*list) = NULL;
        return;
    }

    ListNode* next_node = NULL;

    while (temp_node != NULL)
    {   
        next_node = temp_node->next;

        // Deleting access-mutex
        pthread_mutex_destroy(&(temp_node->access));

        free(temp_node);
        temp_node = next_node;
    }

    free(*list);
    (*list) = NULL;
}

void pushFrontDoubleLinkedList(DoubleLinkedList* list, double* data) {
    ListNode* temp_node = (ListNode*) malloc(sizeof(ListNode));

    // Memory allocating check
    if (temp_node == NULL) {
        fprintf(stderr, "Error in %s: Memory allocation failed\n", __func__);
        exit(1);
    }

    // Configure pointers and value for new element
    temp_node->value = *data;
    temp_node->next = list->head;
    temp_node->prev = NULL;
    temp_node->status = 0;

    if (pthread_mutex_init(&(temp_node->access), NULL) != 0) {
        fprintf(stderr, "Error in %s: Mutex init has failed\n", __func__);
        exit(3);
    }

    // Assigning previous element pointer to the previous head 
    if (list->head) {
        list->head->prev = temp_node;
    }

    // // Assigning new tail if it haven't exist till now
    if (list->tail == NULL) {
        list->tail = temp_node;
    }

    // Applying changes to the list
    list->head = temp_node;
    list->size++;
}

void pushBackDoubleLinkedList(DoubleLinkedList* list, double* data) {
    ListNode* temp_node = (ListNode*) malloc(sizeof(ListNode));

    // Memory allocating check
    if (temp_node == NULL) {
        fprintf(stderr, "Error in %s: Memory allocation failed\n", __func__);
        exit(1);
    }

    // Configure pointers and value for new element
    temp_node->value = *data;
    temp_node->next = NULL;
    temp_node->prev = list->tail;
    temp_node->status = 0;

    // Initializing access-mutex
    if (pthread_mutex_init(&(temp_node->access), NULL) != 0) {
        fprintf(stderr, "Error in %s: Mutex init has failed\n", __func__);
        exit(3);
    }

    // Assigning next element pointer to the previous tail
    if (list->tail) {
        list->tail->next = temp_node;
    }

    // Assigning new head if it haven't exist till now
    if (list->head == NULL) {
        list->head = temp_node;
    }

    // Applying changes to the list
    list->tail = temp_node;
    list->size++;
}

void popFrontDoubleLinkedList(DoubleLinkedList* list) {
    ListNode* popped_node;
    
    // Head existing check
    if (list->head == NULL) {
        fprintf(stderr, "Error in %s: Trying to delete non existing head of the list\n", __func__);
        exit(3);
    }

    // Saving element until changing pointers
    popped_node = list->head;

    // Assigning new head of list
    list->head = list->head->next;

    // If popped element was not a tail make 
    // new head's previous element points to NULL
    if (list->head) {
        list->head->prev = NULL;
    }

    if (popped_node == list->tail) {
        list->tail = NULL;
        list->head = NULL;
    }

    // Deleting access-mutex
    pthread_mutex_destroy(&(popped_node->access));

    // Clearing node from memory
    free(popped_node);

    list->size--;
}

void popBackDoubleLinkedList(DoubleLinkedList* list) {
    ListNode* popped_node;

    // Tail existing check
    if (list->tail == NULL) {
        fprintf(stderr, "Error in %s: Trying to delete non existing tail of the list\n", __func__);
        exit(3);
    }

    // Saving element until changing pointers
    popped_node = list->tail;

    // Assigning new tail of list
    list->tail = list->tail->prev;

    // If popped element was not a head make 
    // new tail's next element points to NULL
    if (list->tail) {
        list->tail->next = NULL;
    }

    if (popped_node == list->head) {
        list->tail = NULL;
        list->head = NULL;
    }

    // Deleting access-mutex
    pthread_mutex_destroy(&(popped_node->access));

    // Clearing node from memory
    free(popped_node);

    list->size--;
};

void printIntDoubleLinkedList(DoubleLinkedList* list, int row_capacity) {
    int counter = 0;
    ListNode* tmp = list->head;

    if (list->size == 0) {
        printf("List is empty. Nothing to print\n");
    }
    else {
        while(tmp) {
            printf("%f ", tmp->value);
            tmp = tmp->next;

            counter++;

            if (counter == row_capacity) {
                counter = 0;
                printf("\n");
            }
        }
        printf("\n");
    }
};

double getRandomDouble (double min, double max) {
    return (((double) rand()/RAND_MAX) * (max - min) + min);    
};

void generateDoubleLinkedList(DoubleLinkedList* list, int size, int* min, int* max) {
    double tmp = 0;

    for (int i = 0; i < size; i++) {
        tmp = getRandomDouble((double) *min, (double) *max);
        pushBackDoubleLinkedList(list, &tmp);
    }
};
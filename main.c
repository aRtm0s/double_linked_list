#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <errno.h>  

#include <time.h>
#include <string.h>
#include <ctype.h>

#include "double_linked_list.h"
#include "double2bin_manip.h"

#define LIST_SIZE 100

typedef struct _ThreadData
{
    int number;
    int mode;  // work mode: 1 - from beginning; 0 - from end
    int count; // number of elements counted in thread
    FILE* log; // log-file  
    DoubleLinkedList* list; // list pointer
} ThreadData;

void* threadFunction (void* sharedData);   
void printLogFile (FILE* dest, char str[]); // writes timestamp and message to log-file

int convertable2Int (char* str); // returns 1 if str could be converted to integer 
                                   // else returns 0

int main (int argc, char* argv[])
{   
    int min, max;

    switch (argc-1)
    {
    case 0:
        fprintf(stderr, "Error with input parameters: input parameters are not set."\ 
            "\nPlease enter two integer numbers or use -h for help\n");
        exit(-1);
        break;

    case 1:
        if (strcmp(argv[1], "-h") == 0) {
            printf("[HELP]\nInput parameters: [-h] min max\n\t-h help "\
            "\n\tmin, max - values for generated double numbers in [min, max]"\
            "\nNOTE: if you want to be 0 for min/max type zero instead of 0.\n");
            return 0;
        } else {
            fprintf(stderr, "Error with input parameters: unknown parameter."\ 
            "\nPlease enter two different integer numbers or use -h for help\n");
            exit (-1);
        }
        break;

    case 2:
        if (strcmp(argv[1], "zero") != 0 && strcmp(argv[2], "zero") != 0 
            && convertable2Int(argv[1]) == 0 && convertable2Int(argv[2]) == 0) {
                fprintf(stderr, "Error with both input parameters: non-convertible to integer."\ 
                "\nPlease enter different integer number or use -h for help\n");
                exit (-1);
            }

        if (strcmp(argv[1], "zero") == 0) {
            min = 0;
        } else {
            if (convertable2Int(argv[1]) == 0) {
                fprintf(stderr, "Error with first parameter: non-convertible to integer."\ 
                "\nPlease enter integer number or use -h for help\n");
                exit (-1);
            } else {
                min = atoi(argv[1]);
            }
        }

        if (strcmp(argv[2], "zero") == 0) {
            max = 0;
        } else {
            if (convertable2Int(argv[2]) == 0) {
                fprintf(stderr, "Error with second parameter: non-convertible to integer."\ 
                "\nPlease enter integer number or use -h for help\n");
                exit (-1);
            } else {
                max = atoi(argv[2]);
            }
        }

        if (min == max) {
            fprintf(stderr, "Error with input parameters: entered values are equal."\ 
            "\nPlease enter two different integer numbers or use -h for help\n");
            exit (-1);
        } 

        if (min > max) {
            fprintf(stderr, "Error with input parameters. "\ 
            "First number should be less than second number.\n");
            exit (-1);
        }
        break;
    
    default:
        fprintf(stderr, "Error with input parameters: too much arguments.\nUse -h for help\n");
        exit (-1);
        break;
    }

    DoubleLinkedList* myList = createDoubleLinkedList();

    // Setting the "seed" for generating pseudo random numbers
    srand((unsigned int) time(NULL));

    printf("\nGenerating list with double numbers in [%d; %d]\n", min, max);

    generateDoubleLinkedList(myList, LIST_SIZE, &min, &max);
    printIntDoubleLinkedList(myList, 1);

    // Threads array
    pthread_t* threads = (pthread_t*) malloc(2 * sizeof(pthread_t));
    if (threads == NULL) {
        fprintf(stderr, "Memory allocation error in %s. Program forced quit...\n", __func__);
        exit(1);
    }

    // Data for threads
    ThreadData* threadData = (ThreadData*) malloc(2 * sizeof(ThreadData));
    if (threadData == NULL) {
        fprintf(stderr, "Memory allocation error in %s. Program forced quit...\n", __func__);
        exit(1);
    }
    
    // Opening/creating log-file for writing
    FILE* fp;
    char name[] = "log_threads.txt";
    if ((fp = fopen(name, "w+")) == NULL) {
        fprintf(stderr, "Failed to open/create file named %s\n", name);
        exit(1);
    }

    fprintf(fp ,"\n\t[THREADS LOG]\n\n");

    for (int i = 0; i < 2; i++) {
        if (i == 0) {   // thread #1
            threadData[i].mode = 1;
        } else {
            threadData[i].mode = 0;
        }
        threadData[i].number = i + 1;
        threadData[i].count = 0;
        threadData[i].list = myList;
        threadData[i].log = fp;

        if (pthread_create(&threads[i], NULL, threadFunction, &threadData[i]) != 0) {
		    fprintf(stderr, "Error in %s: pthread_create for thread#%d was failed\n", __func__, i);
		    exit(-1);
	    }
    }

    // Waiting for all threads
    for (int i = 0; i < 2; i++) {
        pthread_join(threads[i], NULL);
    }

    fprintf(fp ,"\n\t[END THREADS LOG]\n\n");

    printf("Results of processing:\nAmount of\n");
    printf(" - 0's in elements processed by thread#1: %d\n", threadData[0].count);
    printf(" - 1's in elements processed by thread#2: %d\n", threadData[1].count);

    free(threads);
    free(threadData);

    deleteDoubleLinkedList(&myList);

    return 0;
}

// THREAD FUNCTION

void* threadFunction (void* sharedData) {
    ThreadData* thread_data = (ThreadData*) sharedData;
    int counter = 0, counter_mode;
    ListNode* tmp;
    FILE* thread_log = thread_data->log;
    char temp_str[100] = "";
    
    
    sprintf(temp_str, "Thread#%d: starts processing...", thread_data->number);
    printLogFile(thread_log, temp_str);

    if (thread_data->list->size == 0) {
        sprintf(temp_str, "Thread#%d: list is empty, processing ended.", thread_data->number);
        printLogFile(thread_log, temp_str);
        thread_data->count = counter;
        return NULL;
    }

    sprintf(temp_str, "Thread#%d: running with mode %d...", 
        thread_data->number, thread_data->mode);
    printLogFile(thread_log, temp_str);

    sprintf(temp_str, "Thread#%d: current list size is %d",
         thread_data->number, thread_data->list->size);
    printLogFile(thread_log, temp_str);

    tmp = (thread_data->mode == 1) ? thread_data->list->head : thread_data->list->tail;

    while (1) {
        if (thread_data->list->size == 0) {
            sprintf(temp_str, "Thread#%d: list is empty, processing ended.", 
                thread_data->number);
            printLogFile(thread_log, temp_str);

            thread_data->count = counter;
            pthread_exit(0);
        }

        if (tmp->status == 1) {
            sprintf(temp_str, "Thread#%d: node is already processed, processing ended.", 
                thread_data->number);
            printLogFile(thread_log, temp_str);

            thread_data->count = counter;
            pthread_exit(0);
        }

        while( pthread_mutex_trylock(&(tmp->access)) ) {
            sprintf(temp_str, "Thread#%d: waiting for locking mutex...",
                thread_data->number);
            printLogFile(thread_log, temp_str);

            if (thread_data->list->size == 0) {
                sprintf(temp_str, "Thread#%d: list is empty, processing ended.", 
                    thread_data->number);
                printLogFile(thread_log, temp_str);
                    
                thread_data->count = counter;
                pthread_exit(0);
            } 
            
            if (thread_data->list->size == 1 && errno == EBUSY) {
                sprintf(temp_str, "Thread#%d: last element locked by other thread, processing ended.", 
                    thread_data->number);
                printLogFile(thread_log, temp_str);

                thread_data->count = counter;
                pthread_exit(0);
            }
        }

        sprintf(temp_str, "Thread#%d: captured node with data %f", 
            thread_data->number, tmp->value);
        printLogFile(thread_log, temp_str);

        counter_mode = (thread_data->mode == 1) ? 0 : 1; // setting the rule for digit counter
        counter += binDigitCounter(&(tmp->value), counter_mode); // Counting 0's or 1's
        tmp->status = 1;

        pthread_mutex_unlock(&(tmp->access)); // unlocking access-mutex

        sprintf(temp_str, "Thread#%d: node processed, current list size is %d", 
                thread_data->number, thread_data->list->size);
        printLogFile(thread_log, temp_str);

        tmp = (thread_data->mode == 1) ? tmp->next : tmp->prev; // going to next node

        // Deleting processed node
        if (thread_data->mode == 1) {
            popFrontDoubleLinkedList(thread_data->list);
        } else {
            popBackDoubleLinkedList(thread_data->list);
        }

    }

    thread_data->count = counter;

    sprintf(temp_str, "Thread #%d: end processing with result %d", 
                thread_data->number, thread_data->count);
    printLogFile(thread_log, temp_str);

    return NULL;
};

void printLogFile (FILE* dest, char str[]) {
    struct tm tm = *localtime(&(time_t){time(NULL)});
    fprintf(dest, "%.2d:%.2d:%.2d %s\n", tm.tm_hour, tm.tm_min, tm.tm_sec, str);
}

int convertable2Int (char* str) {
    for (int i = 0; str[i] != '\0'; i++) {
        if (isdigit(str[i]) == 0) {
            if (str[i] == '-' && i == 0) {
                continue;
            }
            return 0;
        }
    }

    return 1;
}